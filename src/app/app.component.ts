import { Component, OnDestroy, OnInit } from '@angular/core';

import { Article } from './models/article.model';
import { ArticlesService } from './service/articles.service';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'HN Feed';
  paragraph = 'We <3 hacker news!';

  articles: Article[] = [];

  private subscription: Subscription = new Subscription();

  constructor(public articlesService: ArticlesService) { }

  ngOnInit() {
    this.loadData();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  loadData() {
    this.subscription.add(this.articlesService.loadData()
                .subscribe( (resp: any) => {
                  this.articles = resp.articles;
                }));
  }

  deleteArticle(article: Article) {
    this.articlesService.deleteArticle( article )
                .subscribe( (resp: any) => {
                  console.log(resp);
                });
    this.articles = this.articles.filter(c => c._id !== article._id);
  }

  showStory(url: string) {
    if (url) {
      window.open(url, '_blank');
    }
  }

}
