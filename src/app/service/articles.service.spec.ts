import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ArticlesService } from './articles.service';
import { Article } from '../models/article.model';

import PATH from '../config/config';

describe('ArticlesService', () => {
  let service: ArticlesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ArticlesService]
    });
    service = TestBed.inject(ArticlesService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve posts from API via GET', () => {
    const dummyArticle: Article[] = [
      {
        _id: '1',
        created_at: '20:51 PM',
        title: 'DummyTitle',
        url: null,
        author: null,
        story_title: null,
        story_url: null
      },
      {
        _id: '2',
        created_at: '20:51 PM',
        title: 'DummyTitle',
        url: null,
        author: null,
        story_title: null,
        story_url: null
      }
    ];

    service.loadData().subscribe(resp => {
      expect(resp.length).toBe(2);
      expect(resp).toEqual(dummyArticle);
    });

    const request = httpMock.expectOne(`${PATH}/articles`);

    expect(request.request.method).toBe('GET');

    request.flush(dummyArticle);

  });
});
