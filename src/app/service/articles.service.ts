import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

import { Article } from '../models/article.model';
import PATH from '../config/config';


@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(public http: HttpClient) { }

  loadData() {

    const url = PATH + 'articles';
    return this.http.get( url )
                .pipe(map( (resp: any) => {
                  return resp;
                }),
                catchError( err => {
                  return throwError(err.message);
                }));

  }

  deleteArticle( article: Article ): Observable<{}> {
    const url = PATH + 'article/' + article._id;
    return this.http.delete( url );
  }

  // deleteArticle( article: Article ) {

  //   const url = PATH + 'article/' + article._id;

  //   console.log(article._id);

  //   return this.http.delete( url )
  //               .pipe(map( resp => {
  //                 console.log(resp);
  //                 return true;
  //               }),
  //               catchError( err => {
  //                 return throwError(err.message);
  //               }));

  // }
}
