export class Article {

    constructor(
        public _id: string,
        public created_at: string,
        public title: string,
        public url: string,
        public author: string,
        public story_title: string,
        public story_url: string
    ) { }

}