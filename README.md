# HN Feed - front

_Front-End test for Reign._

## Install Node package

```
npm install
```

## Development server 🔧

_Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files._

## Running unit tests ⚙️

_Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io)._

## Running end-to-end tests 🔩

_Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/)._

## Build 📦

_Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build._

## This project was generated with 🛠️

* [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.
* [NodeJS](https://github.com/nodejs/node) version 12.16.1

## Author ✒️

* **Claudio Stuardo** - [ClaudioStuardo](https://github.com/ClaudioStuardo)

## Further help 📄

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
